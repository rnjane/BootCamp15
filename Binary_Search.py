'''Class BinarySearch inheriting from list'''
class BinarySearch(list):

    '''__init__ with a(length of list) and b(step between consecutive values) as arguments'''
    def __init__(self, a, b):
        self.a = a
        self.b = b

        for i in range(self.a):
            list.append(self, self.b)
            self.b += b

        self.length = self.a

    '''Search Method - gets the index of an item'''

    def search(self, value):
        item_found = False
        end = (self.length - 1)
        start = 0
        count = 0
        try:
            index = self.index(value)
            item_found = True
        except ValueError:
            index = -1
            item_found
        while start <= end and value != self[end] and item_found:
            mid_item = (start + end) // 2
            mid_value = self[mid_item]
            if value > mid_value:
                start = mid_item + 1
                count += 1
            elif value < mid_value:
                end = mid_item - 1
                count += 1
            else:
                count += 1
                break
        return {'count': count, 'index': index}

print(BinarySearch(100, 10).search(880))
