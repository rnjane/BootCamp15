'''This Function will return prime numbers between 0 and n'''
def prime_generator(n):

    '''This function will return true if a number is prime, and false if it isn't'''
    def is_prime(num):
        for i in range(3, int(num ** 0.5) + 1, 2):
            if num % i == 0:
                return False
        return True

    '''Empty List, to store the prime numbers'''
    primes_list = []

    '''Ensure the input is ok'''
    if type(n) != int:
        return 'Only Numbers allowed'
    elif n < 0:
        return 'Only Positive Numbers allowed'
    elif n <= 1:
        return []
    elif n >= 2:
        primes_list.append(2)

    '''Loop through all numbers, appending prime numbers to the list created'''
    for x in range(3, n+1, 2):
        if is_prime(x):
            primes_list.append(x)
    return primes_list

print(prime_generator(100))


