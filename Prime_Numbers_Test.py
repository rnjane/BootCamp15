import unittest
import Prime_Numbers_Check as pn
class test_prime_numbers(unittest.TestCase):

    #Test if input is a list
    def if_list(self):
        self.asertEqual(pn([]), 'Only Integers Accepted')

    '''Test if input is a dictionary'''
    def if_dictionary(self):
        self.assertEqual(pn({}), 'Only Integers Accepted')

    '''Test if input is a tuple'''
    def if_tuple(self):
        self.assertEqual(pn((23,)), 'Only Integers Accepted')

    '''Test if input is none'''
    def if_input_none(self):
        self.assertEqual(pn(None), 'Input cant be empty')

    '''Test if input is string'''
    def if_input_string(self):
        self.assertEqual(pn("Hello"), 'Only Numbers allowed')

    '''Test if input is correct'''
    def if_result_correct(self):
        self.assertEqual(pn(10), [2, 3, 5, 7])

    '''Test if n is included'''
    def if_n_is_included(self):
        self.assertEqual(pn(7), [2, 3, 5, 7])

    '''Test if input is 0'''
    def if_input_zero(self):
        self.assertEqual(pn(0), [])

    '''Test if input is 1'''
    def if_input_one(self):
        self.assertEqual(pn(1), [])

    '''Test if input is -1'''
    def if_input_negative(self):
        self.assertEqual(pn(-1), 'Input cant be negative')
        



