class Car(object):
    num_of_doors=4
    num_of_wheels=4

    '''Constructor for the car class, with initializations'''
    def __init__(self,name='General',model='GM',type='saloon', speed = 0):
        self.name=name
        self.model=model
        self.type=type
        self.speed=speed

        '''Assign number of doors and wheels according to car type'''
        if self.name == 'Porshe' or self.name == 'Koenigsegg':
            self.num_of_doors = 2
        elif self.type == 'trailer':
            self.num_of_wheels = 8
        else:
            self

    '''Method to check if type of car is a saloon'''
    def is_saloon(self):
        if self.type is not 'trailer':
            return True
        return False

    '''Method to check type of car and return its speed'''
    def drive(self, speed):
        if self.type is 'trailer':
            self.speed = speed * 77 / speed
        else:
            self.speed = 10 ** speed
        return self